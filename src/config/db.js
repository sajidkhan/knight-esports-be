const mongoose = require("mongoose");
const connectMongo = () => {
  const connectToDatabase = new Promise((resolve, reject) => {
    mongoose.set("strictQuery", true);
      mongoose.connect(
        process.env.MONGO_URI,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true
        }
      )
        .then(() => {
          const database = mongoose.connection
         
          database.on('error', err => {
            logError(err);
          });
          database.once(
            'open',
            () => {
              console.log('Connected to database...')
              resolve(database)
            }
          )
        })
        .catch(reject)
    })

}
// const connectMongo = () => {
//   mongoose.set("strictQuery", true);
//   mongoose
//     .connect(`${process.env.MONGO_URI}`, {
//       useNewURLParser: true,
//       useUnifiedTopology: true,
//     })
//     .then((conn) => {
//       console.log(`Connected to: ${conn.connection.host}!`);
//     });
// };

module.exports = { connectMongo };
